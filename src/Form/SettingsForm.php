<?php

namespace Drupal\microsoft_teams_notifier\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Microsoft Teams notifier settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'microsoft_teams_notifier_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['microsoft_teams_notifier.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['webhook_url'] = [
      '#title' => $this->t('Webhook url'),
      '#type' => 'textfield',
      '#maxlength' => '255',
      '#required' => TRUE,
      '#description' => $this->t('The microsoft teams webhook url'),
      '#default_value' => $this->config('microsoft_teams_notifier.settings')->get('webhook_url'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('microsoft_teams_notifier.settings')
      ->set('webhook_url', $form_state->getValue('webhook_url'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
