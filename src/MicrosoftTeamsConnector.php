<?php

namespace Drupal\microsoft_teams_notifier;

use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Teams connector.
 */
class MicrosoftTeamsConnector {
  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The request stack service.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The Web hook url variable.
   *
   * @var MicrosoftTeamsConnector
   */
  private $webhookUrl;

  /**
   * Creates microsoft teams connector object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The current request stack service.
   * @param MicrosoftTeamsConnector $webhookUrl
   *   The web hook url service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    RequestStack $request_stack,
    $webhookUrl
  ) {
    $this->configFactory = $config_factory;
    $this->requestStack = $request_stack;
    $this->webhookUrl = $webhookUrl;
  }

  /**
   * Gets card message.
   */
  public function getMessage(string $currentRelease, array $recommendedRelease, ?array $securityRelease) {
    $type = 'recommended';
    $activeTheme = $this->configFactory->get('system.theme')->get('default');
    $logoPath = DRUPAL_ROOT . theme_get_setting('logo.url', $activeTheme);

    // Default image - Drupal logo.
    $image = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKoAAACqCAMAAAAKqCSwAAAAAXNSR0IB2cksfwAAAAlwSFlzAAALEwAACxMBAJqcGAAAADNQTFRFAAAAAJzeAJzeAJzeAJzeAJzeAJzeAJzeAJzeAJzeAJzeAJzeAJzeAJzeAJzeAJzeAJzeim73wgAAABF0Uk5TABCg/yBgcNDgMECwwFDwkIAM8AZKAAAGsklEQVR4nO2d63riOgxFJxhIQrn0/Z92BiiXJFvSlmJj93xHfwtmxdbdsfvnTwnpioxaRDa1AWjp0rY2Aiu7tK+NwEqfhtoIpIwppUNtCE42/1C/akNQsk1X+RWGtbmh/gZ/dZ/UX6Gtxx/UY20QU07pIbvaKIZ0wxP13HgmcEkvadthHdK7nGrjKNKdJ6gtq8BXmkq7XuA7zeW7NpIghwVpq+q6PQPU81gbC0jXA9KUhgZN6whJU+prgy1kI5C2l2Mtjb9V1p1C2lbiopO2xIocapusI3KoM2mjJmBI2wgFFGkTrCRpA6w0aXVWB2llVhdpVVYnaUVWN2k11gBpJVaNdJD/VIFVI/1SstfPs2qkfSfXBJ9ntUilSuvzrCZpM6ywjJ6SNsKqQnTcxz7DSiOQj9QAqf7Rvjyra1mrsjoVsCarNwyprE2R6qwl2y4X+WfFTqoWLsqxak0UuZeusZZ6b+Ak/6TantZYy7Rd1FT6HP1mibDVKfmytZeisJ4L7MYrlpzMnT+FNb97VdzUVSxbVlhz729aHVRzM01hzesGRoNUNyuLNWdHUzepm9heR2bNmRHO906BEPtTMmu+vdi9TZrSxR5HVvhce7FkG4UIPDJrpkige9SXEFu/4vrk2d/Uds8mwtR2onsm9McU00/5WEULzeCx2OUnWcVUe31NIGnXeUS/ybBKRrrWC4gDj3h+CFZJo9bmWJIZXD1Th/5IsEoua13eIs3ATyKFbIRglZ5/lWUJbdKHCUR1QDCtNfFV2JJ+aVWQVWolrphWYVLfwlKQVVDX+LQKkzpR/yCrEAnC04ondRauY6yCE4xO6xaOtshKYqxCUyE4rdinLL1fjBWrQKw1hNcIFRchVsELhEIWtlIYqUOsOLkMla/QTwuHUyCr0YzAqxY5/YKNSqpKIqx42QKlC+ylys8M58hghSV7oByA66+UerBY1FnhtPo1AK6/Ogw0E50VaqtbA2D2r5onrhdUVvh07moAemjtZ6WiSauZ4NK5gytaGzWUiGWz9i04IU5SmP5rPQml/6qwQsNy5gFI8bTGpNrXkFk79HGnsqJJUso0o1MssyINcJaDyEZkp2r1tGVWtHpOz4p+T3R4RKtQYoU24SJ1jUA1NaV4ib7rsitUVUn+TmzATEVQH1QUuQ6+IBUS1lDbmiZYketwuQB+AJxV79BMQ1ZkkS5UlAHiWRH6VlDZ0QgeVYOCNAgqOwxSVyTov4APQWlAEVQYpO6Th1hRuVUCFfwMnLpHpghj83KQEqgk6ctRcO1X8CFXr51Cha7/3aVB1nmhjx53Leo8r4auf2MOMy8LSqDOzAo61KP/M+hpMqOi1V0UUnDLezrz9sP4UacOHLkpUPJBfZ6MVAJ1Eu6Q8cPiFIatUf+7CxUF1vcB0GQJO7owFLw+igKrqwxAy/tWWiEVFBt/UFWef0WBwpWuwPbDiwVUREqLEhngM9NG6+dChRsWT8tFa6pl7shlPT6/OrWGbY+HhqF+s/qKBdSXn8HQD/kaAWgEZbvSWDJkhXcrhcvnIhX2gUZhdNNk0X7KLQNDquosrnFf5+Y5l09BvHgCkG6JC4pmzpaF0IT4xwr+QugWUtd+i2fE2QiSdtcHMD3UXpN92Nnz5BNxHFHjtpqUY00zcZIy79b9CLmBR79Z5G4Fmx2zp7D7d+y0ujfZhL3gpdB5MPvw/q1Ltr9DTwJs+y4lsB1IvV6ZPHs33MMHtthYDeBH1M5qvCSydc2ZgcNeqXUKvW/HmYEDlYoCseMXVBRwREEGNfjaNfXiambU4GuslHdxjM2gRl9hNE5Y3CSvroYPYDH+ymGxhAeIvxfKTCs/mu1XV5xqY/Z5+OzSjlZrXrYlnABtV7aVrnuF2T68QqcXZkhZeTaAsFpWA8xAvfb2O7saIN2V+dCrj4gRlsVNqzWpGU4y2SpATas5TI7LD213SBQC5uGyPAeFTX+44v36h2Q6d6ldXUH+kOWosl0YYZfwoXcq3yTfLZ12QaCyml/PefLaTooGeQlNu8x79p7IsYQIDg87FSSlWAcQC7qimV+cNQ27qcqOxJdK3LzAdRyO34d71nnYbey0rNQdEXxzkJdiN0RErjDT5FzuWs6RWVIHaclLjTq+mW3LsfB1Nmwv05by13PbLp2S/iM3Wn1nsK5P3Xi+Xauxxw/+P5HDGi1AAbik7KJ+a6hwxfEuMrM1QK9yYFKYd/mqeBFzt+entt/X/uc8W4q2PuddutNFw+0vp7b+acBhfznOvcJwvOzbuCccyeEltVH+l/+W/AU6qoldZTE03AAAAABJRU5ErkJggg==";

    $request = $this->requestStack->getCurrentRequest();

    // Encode the theme logo image.
    if (file_exists($logoPath)) {
      $imageData = file_get_contents($logoPath);
      if (!empty(base64_encode($imageData))) {
        // Encode the image data to Base64.
        $image = 'data:image/png;base64,' . base64_encode($imageData);
      }
    }
    $facts = [
      [
        "name" => "Current release:",
        "value" => $currentRelease,
      ],
      [
        "name" => "Recommended release:",
        "value" => sprintf('[%s](%s)', $recommendedRelease['version'], $recommendedRelease['release_link']),
      ],
    ];
    if ($securityRelease !== NULL) {
      $type = 'security';
      $facts[] = [
        "name" => "Security release:",
        "value" => sprintf('[%s](%s)', $securityRelease['version'], $securityRelease['release_link']),
      ];
    }
    return [
      "@type" => "MessageCard",
      "@context" => "https://schema.org/extensions",
      "summary" => "Forge Card",
      "themeColor" => $type === 'security' ? 'F44336' : 'FF9800',
      "title" => $type === 'security' ? "A new security update is available" : 'A new recommended update is available',
      "sections" => [
        [
          "activityTitle" => $this->configFactory->get('system.site')->get('name'),
          "activitySubtitle" => $request->getSchemeAndHttpHost(),
          "activityImage" => $image,
          "facts" => $facts,
        ],
      ],
    ];
  }

  /**
   * Sends card message as POST request.
   *
   * @param string $message
   *   Card message.
   * @param int $curlOptTimeout
   *   By default = 50.
   * @param int $curlOptConnectTimeout
   *   By default = 15.
   *
   * @throws \Exception
   */
  public function send($message, $curlOptTimeout = 50, $curlOptConnectTimeout = 15) {
    $json = json_encode($message, JSON_THROW_ON_ERROR);

    $ch = curl_init($this->webhookUrl);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_TIMEOUT, $curlOptTimeout);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $curlOptConnectTimeout);
    curl_setopt($ch, CURLOPT_HTTPHEADER, [
      'Content-Type: application/json',
      'Content-Length: ' . strlen($json),
    ]);

    $result = curl_exec($ch);

    if (curl_error($ch)) {
      throw new \RuntimeException(curl_error($ch), curl_errno($ch));
    }
    if ($result !== "1") {
      throw new \RuntimeException('Error response: ' . $result);
    }
  }

}
