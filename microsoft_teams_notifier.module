<?php

/**
 * @file
 * Primary module hooks for Microsoft Teams notifier module.
 */

use Drupal\microsoft_teams_notifier\MicrosoftTeamsConnector;

/**
 * Implements hook_cron().
 */
function microsoft_teams_notifier_cron() {
  if ($available = update_get_available(TRUE)) {
    $updateNotifierConfig = \Drupal::configFactory()->getEditable('microsoft_teams_notifier.settings');
    \Drupal::moduleHandler()->loadInclude('update', 'compare.inc');
    $projects = update_calculate_project_data($available);
    $lastNotifiedVersion = $updateNotifierConfig->get('last_notified_version');
    if (isset($projects['drupal']) && $lastNotifiedVersion !== $projects['drupal']['recommended']) {
      $drupalData = $projects['drupal'];

      if (isset($drupalData['existing_version'], $drupalData['recommended'])) {
        $recommended = [
          'version' => $drupalData['recommended'],
          'release_link' => $drupalData['releases'][$drupalData['recommended']]['release_link'] ?? '',
        ];
        $webhookUrl = $updateNotifierConfig->get('webhook_url');
        if (!empty($webhookUrl)) {
          $connector = new MicrosoftTeamsConnector($webhookUrl);
          $connector->send($connector->getMessage($drupalData['existing_version'], $recommended, $drupalData['security updates'][0] ?? NULL));
          $updateNotifierConfig->set('last_notified_version', $recommended['version'])->save();
        }
      }
    }
  }
}
