Microsoft Teams Update Notifier
------------

This module sends notifications to the
Microsoft teams application when a new
Drupal core update is available on your website.
A cron job is used for processing.

Installation steps:

- Set up an incoming webhook in Microsoft Teams,
read the documentation [here](https://learn.microsoft.com/en-us/microsoftteams/platform/webhooks-and-connectors/how-to/add-incoming-webhook?tabs=dotnet)
- Add the webhook url in the configuration form
(path:/admin/config/system/microsoft-teams-notifier)
- Run the cron job
